<?php
namespace App\Transformers;

use App\Schedule;
use League\Fractal\TransformerAbstract;

/**
 * Created by PhpStorm.
 * User: zaid
 * Date: 7/30/17
 * Time: 3:05 PM
 */

class SchedulTransformer extends TransformerAbstract {

    protected $availableIncludes = ['bus', 'arrival_station', 'departure_station'];
    /**
     * @param Schedule $schedule
     * @return array
     * @desc transform schedule
     */
    public function transform (Schedule $schedule ) {
        return [
            'arrival_time'   => $schedule->arrival_time,
            'departure_time' => $schedule->departure_time,
        ];
    }

    /**
     * @param Schedule $schedule
     * @return \League\Fractal\Resource\Item
     * @desc include bus transformer
     */
    public function includeBus (Schedule $schedule ) {
        return $this->item($schedule->bus, new BusTransformer());
    }

    /**
     * @param Schedule $schedule
     * @return \League\Fractal\Resource\Item
     * @desc include arrival station for departure item
     */
    public function includeArrivalStation (Schedule $schedule ) {
        return $this->item($schedule->arrivalStation, new StationTransformer());
    }

    public function includeDepartureStation ( Schedule $schedule ) {
        return $this->item($schedule->departureStation, new StationTransformer());
    }

}