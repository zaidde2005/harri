<?php
namespace App\Transformers;
use App\Bus;
use League\Fractal\TransformerAbstract;

/**
 * Created by PhpStorm.
 * User: zaid
 * Date: 7/30/17
 * Time: 3:17 PM
 */

class BusTransformer extends TransformerAbstract {

    public function transform (Bus $bus) {
        return [
            'id'           => (int)$bus->id,
            'category'     => $bus->category,
            'plate_number' => $bus->plate_number,
        ];
    }
}