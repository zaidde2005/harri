<?php
namespace App\Transformers;
use App\Bus;
use App\Schedule;
use App\Station;
use League\Fractal\TransformerAbstract;

/**
 * Created by PhpStorm.
 * User: zaid
 * Date: 7/30/17
 * Time: 1:48 PM
 */

Class StationTransformer extends TransformerAbstract{

    protected $availableIncludes = ['arrival', 'departure'];
    /**
     * @param Station $station
     * @return array
     * @desc transform Station object for station list
     */
    public function transform(Station $station){
        return [
            'id'        => (int)$station->id,
            'name'      => $station->name,
            'city'      => $station->city,
            'street'    => $station->street,
            'latitude'  => (double)$station->latitude,
            'longitude' => (double)$station->longitude,
        ];
    }

    /**
     * @param Station $station
     * @return \League\Fractal\Resource\Collection
     * @desc include arrival relation to transformer
     */
    public function includeArrival ( Station $station ) {

        return $this->collection($station->arrival, new SchedulTransformer());
    }

    /**
     * @param Station $station
     * @return \League\Fractal\Resource\Collection
     * @desc include departures from the station
     */
    public function includeDeparture ( Station $station ) {
        return $this->collection($station->departure,  new SchedulTransformer());
    }

}