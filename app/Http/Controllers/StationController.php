<?php

namespace App\Http\Controllers;

use App\Station;
use App\Transformers\StationTransformer;

class StationController extends Controller
{
    public function index(){
        return fractal()
            ->collection(Station::all())
            ->transformWith( new StationTransformer())
            ->toArray();
    }
}
