<?php

namespace App\Http\Controllers;

use App\Customer;
use App\Http\Requests\ScheduleRequest;
use App\Schedule;
use App\Station;
use App\Transformers\SchedulTransformer;
use Illuminate\Http\Request;

class ReservationController extends Controller
{
    /**
     * @param Customer $customer
     * @return mixed
     * @desc this method returns all customer reservation.
     */
    public function index ($customer) {
        return $customer = Customer::with(
            [
                'schedule.bus',
                'schedule.departureStation',
                'schedule.arrivalStation'
            ]
        )->select('id')->findOrFail($customer);


        return fractal()
            ->collection($customer->schedule)
            ->transformWith(new SchedulTransformer())
            ->parseIncludes(
                [
                    'bus',
                    'arrival_station',
                    'departure_station'
                ]
            )->toArray();
    }

    /**
     * @param Customer $customer
     * @param ScheduleRequest $request
     * @return $this
     * @desc reserve ticket for customer
     */
    public function store ($customer, ScheduleRequest $request) {

        $customer = Customer::select('id')->findOrFail($customer);

        $customer->schedule()->attach($request->get('schedule_id'));

        $reserve = Schedule::with(
            [
                'bus',
                'arrivalStation',
                'departureStation',
            ]
        )->find($request->get('schedule_id'));

        return fractal()
            ->item($reserve)
            ->transformWith(new SchedulTransformer())
            ->parseIncludes(
                [
                    'bus',
                    'arrival_station',
                    'departure_station'
                ]
            )->toArray();
    }
}
