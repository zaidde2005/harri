<?php

namespace App\Http\Controllers;

use App\Station;
use App\Transformers\StationTransformer;
use Illuminate\Http\Request;

class ScheduleController extends Controller
{
    public  function index(Station $station){

        $station->load([
            'arrival.bus',
            'departure.bus',
            'departure.arrivalStation',
            'departure.departureStation'
        ]);

        return fractal()
            ->item($station)
            ->transformWith( new StationTransformer())
            ->parseIncludes(
                [
                    'arrival.bus',
                    'departure.bus',
                    'departure.arrival_station',
                    'arrival.departure_station'
                ]
            )
            ->toArray();
    }
}
