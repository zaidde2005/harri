<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Schedule extends Model
{
    protected $table = 'schedule';

    public function departureStation () {
        return $this->belongsTo(Station::class, 'departure_station_id');
    }

    public function arrivalStation () {
        return $this->belongsTo(Station::class, 'arrival_station_id');
    }
    public function bus () {
        return $this->belongsTo(Bus::class);
    }
}
