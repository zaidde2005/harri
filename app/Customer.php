<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $table = 'customer';

    protected $hidden = ['password'];

    public function schedule () {
        return $this->belongsToMany(Schedule::class, 'reservation');
    }
}
