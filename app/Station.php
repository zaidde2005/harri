<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Station extends Model
{
    protected $table = 'station';

//    public function arrivalBuses(){
//        return $this->belongsToMany(Bus::class, 'schedule',
//            'arrival_station_id', 'bus_id');
//    }

    public function arrival () {
        return $this->hasMany(Schedule::class, 'arrival_station_id');
    }

    public function departure () {
        return $this->hasMany(Schedule::class, 'departure_station_id');
    }
}
